package newpackage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class MyClass {

	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver","C:\\Users\\ASUS\\Downloads\\chromedriver_win32 (1)\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
        
		String baseUrl = "https://m.myhcl.com/WFH/default.aspx?AppFrom=MP";
        String expectedTitle = "TSMS: My Time Sheet";
        String actualTitle = "";

        // launch Chrome and direct it to the Base URL
        driver.get(baseUrl);
       
        try {
        	//Wait for the user to log in
			TimeUnit.SECONDS.sleep(60);
		} catch (InterruptedException e) { 
			e.printStackTrace();
		}
        
        //get the actual value of the title
        actualTitle = driver.getTitle();

        /**
         * compare the actual title of the page with the expected one and print
         * the result as "Passed" or "Failed"
         */
        if (actualTitle.contentEquals(expectedTitle)){
            System.out.println("Test Passed!");
        } 
        else {
            System.out.println("Test Failed");
        }
       
        //close Chrome
        driver.close();
	}

}
